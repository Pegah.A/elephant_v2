- config.py

This file has the values for the general variables such as SHIFT, DELTA, RATE_INTERVAL,list of features, ... and  also paths to code and data.

- read_slice_pick.py

Goes through the inputdata.txt and reads the flows. By using SHIFT (default is 1) and DELTA (default is 5) values, it breaks the flows into chunks and computes the right label.
To compute the label, for each flow chunk, it counts the number of packets that the flow has within RATE_INTERVAL seconds (default is 5) after the last packet of the chunk.
The loop continues until there are NUM_PER_CLASS (default is 1000) samples per each class.


- read_slice_pick_v2.py

This is a much better version of the previous file.  
With the functions of this file, I first go through ALL the flows in inputdata.txt and get the 9 features, normalize them, and store them both in a txt and a csv file.  
Then, I will select different chunks of this large csv file as training, validation, and test set.

- feature_selection.py

features are: ["protocol", "src_port", "dst_port", "src_ip", "dst_ip", "pkt_arr_time", "pkt_inter_arr_time", "pkt_size", "pkt_l4_data_size"]  
Will generate all the 511 combinations of feature selection and store them as lists in a txt file.  
For example:  
[0, 1, 1, 0, 0, 0, 1, 0, 0] means that we will be using src_port, dst_port, pkt_inter_aa_time.

- make_training_info.py

Goes through the different feature sets and builds a training file based on each feature set.
There are two main functions here. make_training_input_output_csv creates csv files using the pandas library and is recommended.  
make_training_input_output creates txt files which can be harder to work with. 


- simple_NN_model.py

A simple neural network model with Keras.  
The model has 5 fully connected layers which have 512 units each.  
The output layer has 6 units (we have 6 classes) and uses softmax activation.  

The optimizer is Adam and the loss function is sparse categorical crossentropy. 

The NN is run using each of the 511 feature combinations and the acc and loss results are saved in a csv file.  





