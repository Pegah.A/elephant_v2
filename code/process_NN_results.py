 # TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

 # Helper libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
from config import *
import math



TRAIN_DATA_PATH = "files/dataset/train_data/"

def concat_csv_files():

    print ("concatenating all the csv files...")
    total = 511
    start_index = 0
    length = 100
    number_of_chunks = math.ceil(total / length)
    all_csv_files = []

    for i in range(number_of_chunks):
        if i == number_of_chunks - 1:
            end_index = 511
        else:
            end_index = start_index + length

        csv_file_path = os.path.join(SYSTEM_PATH, TRAIN_DATA_PATH, "simple_NN_results_" +
                                                        str(start_index) + "_" + str(end_index) + ".csv")
        all_csv_files.append(csv_file_path)

        start_index = end_index



    # combine all files in the list
    combined_csv = pd.concat([pd.read_csv(f) for f in all_csv_files])
    # export to csv
    combined_csv.to_csv(os.path.join(SYSTEM_PATH,TRAIN_DATA_PATH, "combined_train_results.csv"), index=False, encoding='utf-8-sig')



def sort_csv_file():

    print ("sorting the combined csv file...")
    combined_train_results = pd.read_csv(os.path.join(SYSTEM_PATH, TRAIN_DATA_PATH, "combined_train_results.csv"))
    sorted_combined_train_results = combined_train_results.sort_values('val_acc', ascending=False).reset_index()

    sorted_combined_train_results.to_csv(os.path.join(SYSTEM_PATH, TRAIN_DATA_PATH, "sorted_combined_train_results.csv"), index=False)



def get_top_n_feature_sets(n):

    sorted_combined_train_results = pd.read_csv(os.path.join(SYSTEM_PATH, TRAIN_DATA_PATH, "sorted_combined_train_results.csv"))
    top_n = sorted_combined_train_results.iloc[0:n ,:]
    top_n_index_list = list(top_n['index'])
    return top_n_index_list



if __name__ == "__main__":

    concat_csv_files()
    sort_csv_file()
