import itertools
#from EchoRNN2 import EchoRNN2main
#from confusion_matrix import build_confusion_matrices
#from check_confidence import *

import operator as op
import os
import pandas as pd

from config import *


selected_flows_path = SYSTEM_PATH + "files/selected_flows.txt"
selected_flows_file = open(selected_flows_path, "r").readlines()

selected_flows_output_path = SYSTEM_PATH + "files/selected_flows_output.txt"
selected_flows_output_file = open(selected_flows_output_path, "r").readlines()

line_track_path = SYSTEM_PATH + "files/line_track.txt"
line_track_file = open(line_track_path, "w")



def make_training_input_output(shift = SHIFT, delta = DELTA, features = FEATURES, features_path = FEATURES_PATH):
    num_packets_per_row = delta
    features_line_count = 1

    while features_line_count <512:
        train_output_path = SYSTEM_PATH + "files/train_output.txt"
        train_output_file = open(train_output_path, "w")
        train_output_file.truncate()


        curr_features = features_file.readline().split("\n")[0].split("[")[1].split("]")[0].split(",")
        print (curr_features)

        for i in range(len(curr_features)):
            curr_features[i] = int(curr_features[i])


        line_track_file.write(str(features_line_count))
        line_track_file.write("\n")
        print ("features line count is: ", features_line_count)

        num_of_features = 0
        features_list = []
        for i in range(len(curr_features)):
            if curr_features[i] == 1:
                features_list.append(features[i])
                if i<=4:

                    # if i is less than or equal 4, this feature is flow-specific
                    num_of_features += 1
                elif i>4:

                    # if i is bigger than 4, this feature is packet-specific. That means it appears for every packet
                    num_of_features += num_packets_per_row



        train_path = SYSTEM_PATH+ "files/traindata_" + str(curr_features)+ ".txt"
        train_file = open(train_path, "w")
        train_file.truncate()

        for curr_flow in selected_flows_file:
            curr_flow_array = curr_flow.split("\t")


            for i in range(5): # 5 is the number of features related to the flow info
                if curr_features[i] == 1:
                    train_file.write(curr_flow_array[i])  # if the file contains flow_id as the first element, this should be i+1
                    train_file.write("\t")


            flow_index = 5 # if the file contains flow_id as the first element, this should be 6

            for i in range (num_packets_per_row):

                if curr_features[5] == 1:
                    train_file.write(curr_flow_array[flow_index])
                    train_file.write("\t")

                if curr_features[6] == 1:
                    train_file.write(curr_flow_array[flow_index+1])
                    train_file.write("\t")

                if curr_features[7] == 1:
                    train_file.write(curr_flow_array[flow_index+2])
                    train_file.write("\t")

                if curr_features[8] == 1:
                    train_file.write(curr_flow_array[flow_index+3])
                    train_file.write("\t")

                flow_index += 4  # 4 is the number of features related to packets info

            train_file.write("\n")

        for curr_flow_output in selected_flows_output_file:
            train_output_file.write(curr_flow_output)


        features_line_count +=1
        print ("one round of features done")
        print (features_list)

        train_file.close()
        train_output_file.close()
        EchoRNN2main(num_of_features, curr_features)

        #build_confusion_matrices(features_list, curr_features)
        #check_confidence(curr_features)


        #raw_input("Press Enter to continue...")


def make_training_input_output_csv(shift = SHIFT, delta = DELTA, features = FEATURES, features_path = FEATURES_PATH):

    selected_flows_csv_path = SYSTEM_PATH + "files/selected_flows.csv"
    selected_flows_csv_file = pd.read_csv(selected_flows_csv_path)


    features_file = open(features_path, "r")
    num_packets_per_row = delta
    features_line_count = 1

    while features_line_count < 512:

        curr_features = features_file.readline().split("\n")[0].split("[")[1].split("]")[0].split(",")

        print (curr_features)
        for i in range(len(curr_features)):
            curr_features[i] = int(curr_features[i])

        num_of_features = 0
        features_list = []
        index_list = []
        for i in range(len(curr_features)):
            if curr_features[i] == 1:
                features_list.append(features[i])
                index_list.append(i+1)

                if i <= 4:

                    # if i is less than or equal 4, this feature is flow-specific
                    num_of_features += 1
                elif i > 4:

                    # if i is bigger than 4, this feature is packet-specific. That means it appears for every packet
                    num_of_features += num_packets_per_row



        train_csv_path = SYSTEM_PATH + "files/train_data/traindata_" + str(curr_features) + ".csv"
        train_df =  selected_flows_csv_file.iloc[:,index_list]
        train_df.columns = features_list
        train_df['label'] = selected_flows_csv_file['label']
        train_df.to_csv(train_csv_path)

        features_line_count += 1


if __name__ == "__main__":
    #make_training_input_output(shift = SHIFT, delta = DELTA, features = FEATURES, features_path = FEATURES_PATH)
    make_training_input_output_csv(shift=SHIFT, delta=DELTA, features=FEATURES, features_path=FEATURES_PATH)





