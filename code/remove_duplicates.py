import os

system_path = os.path.normpath(os.getcwd() + os.sep + os.pardir) + "/"
features_path = system_path + "files/features.txt"

def remove_duplicates():
    features_file = open(features_path, "r")
    available_files_path_list = []

    for i in range(511):  # (511)
        features_line = features_file.readline().split("\n")[0]
        available_files_path_list.append(system_path + "files/" + "confident_flows_" + str(features_line) + ".txt")

    for path in available_files_path_list:
        seen_ids = []

        my_file = open(path, "r")
        my_file_lines = my_file.readlines()
        my_file.close()
        my_path = path.split(".")[0] + "_unique_flows.txt"
        my_file = open(my_path, "w")
        my_file.truncate()

        for line in my_file_lines:
            flow_id = line.split("\t")[0]
            if flow_id in seen_ids:
                continue
            else:
                seen_ids.append(flow_id)
                my_file.write(line)

        my_file.close()

def count_unique_confident_flows():
    features_file = open(features_path, "r")
    features = features_file.readlines()

    feature_set_unique_confident_count_path = system_path + "/files/" + "feature_set_unique_confident_count.txt"
    feature_set_unique_confident_count_file = open(feature_set_unique_confident_count_path, "w")

    length_list_unique = []
    length_list_nonunique = []

    for feature_set in features:

        unique_flows_path = system_path + "/files/" + "confident_flows_" + str(feature_set.split("\n")[0]) + "_unique_flows.txt"
        unique_flows_file = open(unique_flows_path, "r")
        unique_flows_lines = unique_flows_file.readlines()

        feature_set_unique_confident_count_file.write(feature_set.split("\n")[0])
        feature_set_unique_confident_count_file.write("\t")
        feature_set_unique_confident_count_file.write(str(len(unique_flows_lines)))
        feature_set_unique_confident_count_file.write("\n")


        length_list_unique.append(len(unique_flows_lines))

        """
        nonunique_flows_path = system_path + "/files/" + "confident_flows_" + str(feature_set.split("\n")[0]) + ".txt"
        nonunique_flows_file = open(nonunique_flows_path, "r")
        nonunique_flows_lines = nonunique_flows_file.readlines()
        length_list_nonunique.append(len(nonunique_flows_lines))
        """

    print length_list_unique
    #print length_list_nonunique






if __name__ == "__main__":
    #remove_duplicates()

    # So far, I have removed all the duplicate flows.
    # So for each feature set, I know which individual flows where classified correctly.

    # Now, I can count them to see how different they are

    count_unique_confident_flows()




