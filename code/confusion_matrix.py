from __future__ import division
import numpy as np
import os
import operator
import math

system_path = os.path.normpath(os.getcwd() + os.sep + os.pardir) + "/"


features_list = ["protocol", "src_port", "dst_port", "src_ip", "dst_ip", "pkt_arr_time", "pkt_inter_arr_time",
                "pkt_size", "pkt_l4_data_size"]


def build_confusion_matrices(features_list, curr_features):
    confusion_matrix_weights_dict = {0: 1.0, 1: 0.5, 2: 0.25, 3: 0.125, 4: 0.0625, 5: 0.03125}

    predictions_path = system_path + "files/" + str(curr_features)+ "_predictions.txt" # the predicted results of our model
    predictions_file = open(predictions_path, "r+")

    train_output_rate_path = system_path+ "files/train_output.txt" #the actual true classes of the training data
    train_output_file = open(train_output_rate_path, "r")
    train_output_file_temp = open(train_output_rate_path, "r")

    confusion_matrices_path  = system_path+ "files/confusion_matrices_with_weight.txt"
    confusion_matrices_file = open(confusion_matrices_path, "a")


    total_series_length = len(train_output_file_temp.readlines())
    total_series_length = (total_series_length // 100) * 100  # we are doing this so we would always be using a 10* number of rows => batch_size 10 is divisable
    num_classes = 6
    
    print "in the confusion matrix"
    #print " total_series_length  is:" , total_series_length
    confusion_matrix = np.zeros([num_classes, num_classes])
    for i in range(total_series_length):
        predicted_res = int(predictions_file.readline().split("\n")[0]) - 1  # minus 1 because they need to be array index starting from 0
        actual_res = int(train_output_file.readline().split("\n")[0]) - 1  # minus 1 because they need to be array index starting from 0

        confusion_matrix[predicted_res, actual_res] = confusion_matrix[predicted_res, actual_res] + 1

    percentage_confusion_matrix = confusion_matrix / sum(confusion_matrix)
    sum_diagonal_values = sum(percentage_confusion_matrix.diagonal())
    digonal_avg = sum_diagonal_values / 6
    avg_class_5_6_values =  (percentage_confusion_matrix[4,4] + percentage_confusion_matrix[5,5]) / 2

    weighted_acc = 0

    for i in range(num_classes):
        for j in range(num_classes):
            key = int(math.fabs(i - j))
            weighted_acc += percentage_confusion_matrix[i, j] * confusion_matrix_weights_dict[key]


    #print "The Confusion Matrix is: \n " , confusion_matrix
    #print "\n\n"
    #print "The total number of flows for each class is: \n" , sum(confusion_matrix)
    #print "\n\n"
    #print "The relative Confusion Matrix is: \n",percentage_confusion_matrix

    confusion_matrices_file.write(str(features_list))
    confusion_matrices_file.write("\n")
    confusion_matrices_file.write(str(confusion_matrix))
    confusion_matrices_file.write("\n")
    confusion_matrices_file.write(str(sum(confusion_matrix)))
    confusion_matrices_file.write("\n")
    confusion_matrices_file.write(str(percentage_confusion_matrix ))
    confusion_matrices_file.write("\n")
    confusion_matrices_file.write(str(digonal_avg))
    confusion_matrices_file.write("\n")
    confusion_matrices_file.write(str(avg_class_5_6_values))
    confusion_matrices_file.write("\n")
    confusion_matrices_file.write(str(weighted_acc))
    confusion_matrices_file.write("\n")

    print "writing the confusion matrices part done"
    #confusion_matrices_file.close()



def sort_confusion_matrices():

    # I have a text file that has the confusion matrix for each feature set
    # I have the diagonal percentage and also percentage for class 5 and 6
    # I will sort based on each of them separately

    confusion_matrices_path = system_path + "files/confusion_matrices_with_weight.txt"
    confusion_matrices_file = open(confusion_matrices_path, "r")

    sorted_diagonal_path = system_path + "files/sorted_confusion_matrices_diagonal_acc.txt"
    sorted_diagonal_file = open(sorted_diagonal_path, "w")

    sorted_diagonal_5_6_path = system_path + "files/sorted_confusion_matrices_diagonal_5_6_acc.txt"
    sorted_diagonal_5_6_file = open(sorted_diagonal_5_6_path, "w")

    sorted_by_weight_path = system_path + "files/sorted_confusion_matrices_weighted_acc.txt"
    sorted_by_weight_file = open(sorted_by_weight_path, "w")


    # first line is feature set
    # next 6 lines are the confusion matrix
    # line 8 is sum
    # next 6 lines are percentage confusion matrix
    # line 15 is diagonal
    # line 16 is diagonal 5 and 6


    features_accuracy_diagonal_acc_dict = {}
    features_accuracy_diagonal_5_6_acc_dict = {}
    features_accuracy_weighted_acc_dict = {}

    for i in range(510):  # i want it to happen 511 times. Starts from 0

        features_set = confusion_matrices_file.readline().strip()

        for i in range(13):
            confusion_matrices_file.readline()

        diagonal_acc = confusion_matrices_file.readline().strip()
        diagonal_5_6_acc = confusion_matrices_file.readline().strip()
        weighted_acc = confusion_matrices_file.readline().strip()

        features_accuracy_diagonal_acc_dict[features_set] = float(diagonal_acc)
        features_accuracy_diagonal_5_6_acc_dict[features_set] = float(diagonal_5_6_acc)
        features_accuracy_weighted_acc_dict[features_set] = float(weighted_acc)


    sorted_diagonal_acc_list = sorted(features_accuracy_diagonal_acc_dict.items(), key = operator.itemgetter(1))
    sorted_diagonal_5_6_acc_list = sorted(features_accuracy_diagonal_5_6_acc_dict.items(), key = operator.itemgetter(1))
    sorted_weighted_acc_list = sorted(features_accuracy_weighted_acc_dict.items(), key=operator.itemgetter(1))



    for i in range(510):

        sorted_diagonal_file.write(sorted_diagonal_acc_list[i][0])
        sorted_diagonal_file.write("\n")
        sorted_diagonal_file.write(str(sorted_diagonal_acc_list[i][1]))
        sorted_diagonal_file.write("\n")

        sorted_diagonal_5_6_file.write(sorted_diagonal_5_6_acc_list[i][0])
        sorted_diagonal_5_6_file.write("\n")
        sorted_diagonal_5_6_file.write(str(sorted_diagonal_5_6_acc_list[i][1]))
        sorted_diagonal_5_6_file.write("\n")


        sorted_by_weight_file.write(sorted_weighted_acc_list[i][0])
        sorted_by_weight_file.write("\n")
        sorted_by_weight_file.write(str(sorted_weighted_acc_list[i][1]))
        sorted_by_weight_file.write("\n")
















if __name__ == "__main__":

    """

    features_path = system_path + "files/features.txt"
    features_file = open(features_path, "r")
    features = features_file.readlines()

    for features_binary in features:
        features_binary_list = features_binary.strip().split("[")[1].split("]")[0].split(",")
        features_name = []
        for i in range(len(features_binary_list)):
            if features_binary_list[i] == " 1" or features_binary_list[i] == "1":
                features_name.append(features_list[i])

        build_confusion_matrices(features_name, features_binary.strip())
    """

    sort_confusion_matrices()
