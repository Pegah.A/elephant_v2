 # TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

 # Helper libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
from config import *
import math


def simple_model(start_index, end_index, curr_features_list):

    results_df_path = os.path.join(SYSTEM_PATH, "files/dataset/train_data/simple_NN_results_" + str(start_index) + "_" + str(end_index) + ".csv")

    results_dict = {'features' : [] , 'train_acc': [], 'train_loss': [], 'val_acc': [], 'val_loss': []}

    for i in range(start_index, end_index):
        print ("------------ i is: ", i)

        curr_features = curr_features_list[i]

        print ("current feature selection is: ", curr_features)

        for j in range(len(curr_features)):
            curr_features[j] = int(curr_features[j])

        results_dict['features'].append(curr_features)

        file_features = str(curr_features)

        train_file = pd.read_csv(os.path.join(SYSTEM_PATH, "files/dataset/train_data/train_data_" + file_features + ".csv"))
        train_data =  train_file.iloc[:, :-1]
        train_labels = train_file['label']
        train_labels = train_labels.apply(lambda x: x - 1)

        validation_file = pd.read_csv(os.path.join(SYSTEM_PATH, "files/dataset/validation_data/validation_data_" + file_features + ".csv"))
        validation_data =  validation_file.iloc[:, :-1]
        validation_labels = validation_file['label']
        validation_labels = validation_labels.apply(lambda x: x - 1)

        model = keras.Sequential([
        keras.layers.Flatten(input_shape=(train_data.shape[1],)),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(6, activation=tf.nn.softmax) # because we have 6 classes
        ])

        print (model.summary())

        model.compile(optimizer ='adam' , loss = 'sparse_categorical_crossentropy' , metrics =['accuracy'] )

        #Start training
        history_callback = model.fit(train_data, train_labels, validation_data=(validation_data, validation_labels), epochs=50, shuffle=True)
        train_loss_history = history_callback.history["loss"][-1]
        train_acc_history = history_callback.history["acc"][-1]
        validation_loss_history = history_callback.history["val_loss"][-1]
        validation_acc_history = history_callback.history["val_acc"][-1]

        results_dict['train_acc'].append(train_acc_history)
        results_dict['train_loss'].append(train_loss_history)
        results_dict['val_acc'].append(validation_acc_history)
        results_dict['val_loss'].append(validation_loss_history)

        tf.keras.backend.clear_session() # Destroys the current TF graph and creates a new one.
                                         # Useful to avoid clutter from old models / layers.



    results_df = pd.DataFrame.from_dict(results_dict)
    columns = ['features', 'train_acc', 'train_loss', 'val_acc', 'val_loss']
    results_df = results_df[columns]
    results_df.to_csv(results_df_path, index=False)



if __name__ == "__main__":

    total = 511
    start_index = 500
    length = 100

    number_of_chunks = math.ceil(total/length)
    print ("number of chunks is: ", number_of_chunks)

    features_file = open(FEATURES_PATH, "r")
    curr_features_list = features_file.readlines()
    curr_features_list = [x.split("\n")[0].split("[")[1].split("]")[0].split(",") for x in curr_features_list]

    for i in range(number_of_chunks):
        #if i == number_of_chunks - 1:
        end_index = 511
        #else:
            #end_index = start_index + length
        print ("started chunk " , i)
        print ("calling simple model with: ", start_index, ",", end_index)

        simple_model(start_index, end_index, curr_features_list)

        start_index = end_index