 # TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

 # Helper libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
from config import *
from process_NN_results import *
import math




def medium_model(top_n_feature_set_index_list, curr_features_list):

    n =len(top_n_feature_set_index_list)

    results_df_path = os.path.join(SYSTEM_PATH, "files/train_data/medium_NN_top_" +str(n) + "_results.csv")

    results_dict = {'features' : [] , 'acc': [], 'loss': []}

    for index in top_n_feature_set_index_list:
        print ("------------ index is: ", index)

        curr_features = curr_features_list[index]

        print ("current feature selection is: ", curr_features)

        for j in range(len(curr_features)):
            curr_features[j] = int(curr_features[j])

        results_dict['features'].append(curr_features)

        file_features = str(curr_features)
        train_file = pd.read_csv(os.path.join(SYSTEM_PATH, "files/train_data/traindata_" + file_features + ".csv"))
        train_data =  train_file.iloc[:, :-1]
        train_labels = train_file['label']
        train_labels = train_labels.apply(lambda x: x - 1)

        model = keras.Sequential([
        keras.layers.Flatten(input_shape=(train_data.shape[1],)),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(512, activation=tf.nn.relu),
        keras.layers.Dense(6, activation=tf.nn.softmax) # because we have 6 classes
        ])

        print (model.summary())

        model.compile(optimizer ='adam' , loss = 'sparse_categorical_crossentropy' , metrics =['accuracy'] )

        #Start training
        history_callback = model.fit(train_data, train_labels, epochs=50)
        loss_history = history_callback.history["loss"][-1]
        acc_history = history_callback.history["acc"][-1]

        results_dict['acc'].append(acc_history)
        results_dict['loss'].append(loss_history)

        tf.keras.backend.clear_session() # Destroys the current TF graph and creates a new one.
                                         # Useful to avoid clutter from old models / layers.



    results_df = pd.DataFrame.from_dict(results_dict)
    columns = ['features', 'acc', 'loss']
    results_df = results_df[columns]
    results_df.to_csv(results_df_path, index=False)



if __name__ == '__main__':

    features_file = open(FEATURES_PATH, "r")
    curr_features_list = features_file.readlines()
    curr_features_list = [x.split("\n")[0].split("[")[1].split("]")[0].split(",") for x in curr_features_list]

    top_n_feature_set_index_list = get_top_n_feature_sets(10)
    medium_model(top_n_feature_set_index_list, curr_features_list)
