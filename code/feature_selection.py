import itertools
import operator as op
import os


from config import *

def feature_selection(features = FEATURES, num_packets_per_row = DELTA, features_path = FEATURES_PATH):

    features_file = open(features_path, "w")

    features_index = [0, 1, 2, 3, 4, 5, 6, 7, 8]

    for L in range(0, len(features_index) + 1):
        for subset in itertools.combinations(features_index, L):  # subset is the set of features that have been selected in this round
            features_list = []
            num_of_features = 0
            #features_bool = [False, False, False, False, False, False, False, False, False]
            features_bool = [0,0,0,0,0,0,0,0,0]
            if len(subset) == 0:
                continue
            else:
                for item in subset:
                    if item <= 4:
                        num_of_features += 1
                    elif item > 4:
                        num_of_features += num_packets_per_row
                    features_bool[int(item)] = 1  # by this point, we have decided which features to use this time
                    features_list.append(features[item])
            print ("features are: ", features_list)
            #num_of_lines = ncr(9,len(subset)) * 6 * num_per_class

            features_file.write(str(features_bool))
            features_file.write("\n")




def ncr(n, r):
    r = min(r, n-r)
    if r == 0: return 1
    numer = reduce(op.mul, xrange(n, n-r, -1))
    denom = reduce(op.mul, xrange(1, r+1))
    return numer//denom


if __name__ == "__main__":
    feature_selection(features = FEATURES, num_packets_per_row = DELTA, features_path = FEATURES_PATH)
