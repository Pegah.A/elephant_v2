

import os
system_path = system_path = os.path.normpath(os.getcwd() + os.sep + os.pardir) + "/"

features_list = ["protocol", "src_port", "dst_port", "src_ip", "dst_ip", "pkt_arr_time", "pkt_inter_arr_time",
                "pkt_size", "pkt_l4_data_size"]



def check_confidence(features_list):


    confidences_path = system_path + "files/" + str(features_list) + "_confidence_prediction.txt"
    confidences_file = open(confidences_path, "r")

    selected_flows_path = system_path + "files/selected_flows_with_id.txt"
    selected_flows_file= open(selected_flows_path, "r")
    selected_flows_lines = selected_flows_file.readlines()

    selected_flows_complete_info_path= system_path + "files/selected_flows_complete_info.txt"
    selected_flows_complete_info_file= open(selected_flows_complete_info_path, "r")
    selected_flows_complete_info_lines = selected_flows_complete_info_file.readlines()

    CONFIDENCE_THRESHOLD = 0.6
    num_classes = 6
    num_per_class = 100
    number_of_rows = num_classes * num_per_class

    confident_flows_path =  system_path + "files/confident_flows_" + str(features_list)+".txt"
    confident_flow_chunks_path= system_path + "files/confident_flow_chunks_" + str(features_list)+".txt"

    confident_flows_file = open(confident_flows_path, "w")
    confident_flow_chunks_file = open(confident_flow_chunks_path,"w")

    confident_flows_file.truncate()
    confident_flow_chunks_file.truncate()


    for i in range (number_of_rows):
        confidence = confidences_file.readline().split("\t")[1]
        if float(confidence)>= CONFIDENCE_THRESHOLD:
            confident_flows_file.write(selected_flows_complete_info_lines[i])
            confident_flows_file.write("\n")
            confident_flow_chunks_file.write(selected_flows_lines[i])
            confident_flow_chunks_file.write("\n")



def check_prediction_vs_actual(features_list):


    # For every feature set, I have the predictions and the confidency from the RNN and also the correct label from train_output file
    # I want to read these files at the same time, as well as reading the "selected_flows_with_id"  file.
    # And I want to compare the prediction with the actual label, wherever the prediction == actual, I want to keep that flow form "complete_info" file
    # Finally, I want to run remove duplicates.



    confidences_path = system_path + "files/" + str(features_list) + "_confidence_prediction.txt"
    confidences_file = open(confidences_path, "r")
    confidences_lines = confidences_file.readlines()

    actual_labels_path = system_path + "files/train_output.txt"
    actual_labels_file = open(actual_labels_path, "r")
    actual_labels_lines = actual_labels_file.readlines()

    selected_flows_complete_info_path = system_path + "files/selected_flows_complete_info.txt"
    selected_flows_complete_info_file = open(selected_flows_complete_info_path, "r")
    selected_flows_complete_info_lines = selected_flows_complete_info_file.readlines()

    confident_flows_path = system_path + "files/confident_flows_" + str(features_list) + ".txt"
    confident_flows_file = open(confident_flows_path, "w")
    confident_flows_file.truncate()


    for i in range(len(confidences_lines)):
        prediction = int(confidences_lines[i].split("\t")[0])
        confidence = float(confidences_lines[i].split("\t")[1])

        actual = int(actual_labels_lines[i])

        if prediction == actual:
            confident_flows_file.write(str(confidence))
            confident_flows_file.write("\t")
            confident_flows_file.write(selected_flows_complete_info_lines[i])
            confident_flows_file.write("\n")



if __name__ == "__main__":

    features_path = system_path + "files/features.txt"
    features_file = open(features_path, "r")
    features = features_file.readlines()

    for features_binary in features:
        features_binary_list = features_binary.strip().split("[")[1].split("]")[0].split(",")
        features_name = []
        for i in range(len(features_binary_list)):
            if features_binary_list[i] == " 1" or features_binary_list[i] == "1":
                features_name.append(features_list[i])

        check_prediction_vs_actual(features_binary.strip())