import os


SYSTEM_PATH = os.path.normpath(os.getcwd() + os.sep + os.pardir) + "/"

DATA_PATH = SYSTEM_PATH + "files/inputdata.txt"

SHIFT = 1  # Ihsan: 7
DELTA = 5 # Ihsan: 16
RATE_INTERVAL = 5
NUM_PER_CLASS = 1000
FEATURES = ["protocol", "src_port", "dst_port", "src_ip", "dst_ip", "pkt_arr_time", "pkt_inter_arr_time",
                "pkt_size", "pkt_l4_data_size"]


FEATURES_PATH= SYSTEM_PATH + "files/features.txt"