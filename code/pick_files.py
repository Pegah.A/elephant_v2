
import itertools
import numpy as np
from remove_duplicates import *

system_path = "/Users/pegah/Documents/Pegah/ElephantFlow_Third_Feature(2)/"
features_path = system_path + "files/features.txt"
third_feature_real_index =0

NUM_OF_FLOW_FEATURES = 18

all_features_index_map_dict={"protocol" : 1 , "src_port" : 10 , "dst_port" : 11 , "src_ip" : 5 , "dst_ip" : 9, \
                             "pkt_arr_time" : 20 , "pkt_inter_arr_time" : 21 , "pkt_size" : 22 , "pkt_l4_dara_size" : 23}

# threshold for deciding which flows should be trained by A and which ones should be trained by B
PKT_SIZE_THRESHOLD = 1000


THIRD_FEATURE = "pkt_size"
FEATURE_COMBINATION_SIZE = 1

def main():
    feature_file = open (features_path , "r")
    feature_lines = feature_file.readlines()

    available_files_path_list = []
    for feature_line in feature_lines:
        feature_line = feature_line.strip()
        available_files_path_list.append(system_path + "files/confident_flows_" + str(feature_line)+ "_5_1_unique_flows.txt")

    feature_pkt_size_as_third(available_files_path_list)

######
# WHERE SHOULD I HAVE THIS FUNCTION

def pick_flows_based_on_third_feature(threshold, third_feature):
    all_flows_complete_info_path = system_path + "/files/selected_flows_complete_info_5_1.txt"
    all_flows_complete_info_file = open(all_flows_complete_info_path, "r")
    flows = all_flows_complete_info_file.readlines()

    third_feature_index = all_features_index_map_dict[THIRD_FEATURE]

    set_A_flows= []
    set_B_flows= []

    set_A_flows_path = system_path + "/files/" + "set_A_ " + third_feature + ".txt"
    set_A_flows_file  = open(set_A_flows_path ,"w")

    set_B_flows_path = system_path + "/files/" + "set_B_ " + third_feature + ".txt"
    set_B_flows_file = open(set_B_flows_path, "w")


    for flow in flows:

        if flow == "\n":
            continue
        #   this_feature_in_this_flow is a list of this feature values for each of the packets within this flow
        print "flow is: ", flow
        this_feature_in_this_flow = feature_type_is_packet(flow, third_feature_index - (NUM_OF_FLOW_FEATURES+1))
        mean_of_feature = np.mean(this_feature_in_this_flow)
        if mean_of_feature >= threshold:
            set_A_flows.append(flow)
            set_A_flows_file.write(flow)

        else:
            set_B_flows.append(flow)
            set_B_flows_file.write(flow)




    # I have the list of all the selected flows in selected_flows_complete_info
    # i need two seperate files, one with flow information of ones with mean packet size of more than threshold = 1000
    # another one containing flows of mean packet size of less than threshold

    # so in a loop, keep reading the flows, read each packet's size, have a mean size of the packets of this flow,
    # if bigger than threshold, add it to the first file, if smaller, add it to the second one



def feature_type_is_packet(flow,index):
    print "here flow is: ", flow

    # every line of the files is the information of one flow
    packets = flow.split(";")


    pkt_count = int(packets[0].split("\t")[12])

    this_feature_in_this_flow = []

    for i in range(1, pkt_count+1):
        #print "size is: " , float(packets[i].split("\t")[index+1])
        this_feature_in_this_flow.append(float(packets[i].split("\t")[index+1]))

    return this_feature_in_this_flow



def feature_pkt_size_as_third(available_files_path_list):

    third_feature_index = all_features_index_map_dict[THIRD_FEATURE]

    files_index = [0,1,2,3,4,5,6,7,8]  # because we have 9 possible options for feature_combination of size 1

    #if third_feature_index < 18:
     #   third_feature_type = "flow"
    #else:
    #  third_feature_type = "packet"

    as_third_path = system_path + "files/" + "feature_pkt_size_as_third.txt"
    as_third_file = open(as_third_path, "w")
    as_third_file.truncate()

    for subset in itertools.combinations(files_index,2):  # subsets of length 2 of the 9 possible

        path1 = available_files_path_list[subset[0]]
        path2 = available_files_path_list[subset[1]]

        print "path 1 is: ", path1
        print "path 2 is: ", path2

        feature_set_1 = path1.split("_")[4]
        feature_set_2 =path2.split("_")[4]
        print "feature set 1 is: ", feature_set_1
        print "feature set 2 is: " , feature_set_2

        file1 = open(path1, "r")  # file1 is the file containing flows that subset[0] worked good on them
        file2 = open(path2, "r")  # file 2 is the file containing flows that subset[1] worked good on them



        file1_pkt_size_list = []
        file2_pkt_size_list = []

        file1_lines = file1.readlines()
        file2_lines = file2.readlines()

        if len(file1_lines) == 0 or len(file2_lines) == 0:
            continue

        for line in file1_lines:
            if line == "\n": continue
            this_feature_in_this_flow= feature_type_is_packet(line, third_feature_index - (NUM_OF_FLOW_FEATURES+1))
            file1_pkt_size_list.append(np.mean(this_feature_in_this_flow))
        print "file 1 pkt size list: ", file1_pkt_size_list

        for line in file2_lines:
            if line == "\n": continue
            this_feature_in_this_flow= feature_type_is_packet(line,third_feature_index - (NUM_OF_FLOW_FEATURES + 1))
            file2_pkt_size_list.append(np.mean(this_feature_in_this_flow))
        print "file 2 pkt size list: ", file2_pkt_size_list

        if len(file1_pkt_size_list)== 0:
            file1_pkt_size_mean = "nan"
        else:
            file1_pkt_size_mean = np.mean(file1_pkt_size_list)

        if len(file2_pkt_size_list)== 0:
            file2_pkt_size_mean = "nan"
        else:
            file2_pkt_size_mean = np.mean(file2_pkt_size_list)

        print "file 1 mean is: " , file1_pkt_size_mean
        print "file 2 mean is: ", file2_pkt_size_mean

        as_third_file.write("file 1 feature set: " + feature_set_1)
        as_third_file.write("\n")
        as_third_file.write(str(file1_pkt_size_mean))
        as_third_file.write("\n")
        as_third_file.write("file 2 feature set: " + feature_set_2)
        as_third_file.write("\n")
        as_third_file.write(str(file2_pkt_size_mean))
        as_third_file.write("\n")

        #raw_input("Press Enter to continue...")


#pick_flows_based_on_third_feature(PKT_SIZE_THRESHOLD, "pkt_size")#


main()

